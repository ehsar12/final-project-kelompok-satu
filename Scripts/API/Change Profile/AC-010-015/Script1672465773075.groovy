import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.FormDataBodyParameter
import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.TestObjectProperty
import com.kms.katalon.core.testobject.impl.HttpFormDataBodyContent
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI


WebUI.comment('TESTCASE: ' + testcase_id + ' - ' + scenario)

BaseFileDir = System.getProperty('user.dir')

FilePath = (BaseFileDir + '/Data Files/Website/Change Profile/')

token = CustomKeywords.'com.helper.api.Token.getToken'()
println(token)

RequestObject ro = findTestObject('Object Repository/API/Change Profile/Rest_withoutBody', ['token' : token])

def httpheader = ro.getHttpHeaderProperties()

httpheader.add(new TestObjectProperty('Content-Type', ConditionType.EQUALS, 'multipart/form-data'))

ro.setHttpHeaderProperties(httpheader)

if (testcase_id == '013') {
	value = FilePath+value
}

def formdataparam = new FormDataBodyParameter(name, value, type)


ro.setBodyContent(new HttpFormDataBodyContent([formdataparam]))

response = WS.sendRequest(ro)

statusCode = WS.getResponseStatusCode(response)

if(statusCode == 200) {
	assert true
} else {
	println(statusCode)
	assert false
}

