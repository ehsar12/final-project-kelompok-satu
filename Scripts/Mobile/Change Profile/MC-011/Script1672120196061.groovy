import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

not_run: Mobile.callTestCase(findTestCase('Mobile/Change Profile/TCP_Login'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.comment('Edit photo profile dengan hasil jepretan kamera handphone')

Mobile.setText(findTestObject('Object Repository/Mobile/Profile Page/input_fullname'), 'Algifahri', 2)

Mobile.setText(findTestObject('Object Repository/Mobile/Profile Page/input_phone'), '08123456789', 2)

Mobile.tap(findTestObject('Object Repository/Mobile/Profile Page/input_birthdate'), 2)

String birthdate = '27-Jul-2005'

String[] date = birthdate.split('-')

GlobalVariable.day_datepicker = (date[0])

String month = date[1]

int year = Integer.parseInt(date[2])

int active_year = Integer.parseInt(Mobile.getAttribute(findTestObject('Object Repository/Mobile/Datepicker/span_active_year'), 
        'text', 2))

String active_date = Mobile.getAttribute(findTestObject('Object Repository/Mobile/Datepicker/span_active_fulldate'), 'text', 
    2)

String[] date_2 = active_date.split(' ')

String month_2 = date_2[2]

if (year < active_year) {
    Mobile.tap(findTestObject('Object Repository/Mobile/Datepicker/span_active_year'), 2)

    int decrease = active_year

    while (true) {
        Mobile.scrollToText(String.valueOf(decrease))

        decrease--

        if (decrease == year) {
            GlobalVariable.year_datepicker = year

            Mobile.tap(findTestObject('Object Repository/Mobile/Datepicker/button_choosen_year'), 2)

            break
        }
    }
} else if (year > active_year) {
    Mobile.tap(findTestObject('Object Repository/Mobile/Datepicker/span_active_year'), 2)

    int increase = active_year++

    while (true) {
        Mobile.scrollToText(String.valueOf(increase))

        increase++

        if (increase == year) {
            GlobalVariable.year_datepicker = year

            Mobile.tap(findTestObject('Object Repository/Mobile/Datepicker/button_choosen_year'), 2)

            break
        }
    }
}

def choosen_month = GlobalVariable.month_list.findIndexOf({ 
        it == month
    })

def active_month = GlobalVariable.month_list.findIndexOf({ 
        it == month_2
    })

int index_active_month = active_month

if (active_month > choosen_month) {
    while (true) {
        index_active_month--

        Mobile.tap(findTestObject('Object Repository/Mobile/Datepicker/button_prev_month'), 2)

        if (index_active_month == choosen_month) {
            break
        }
    }
} else if (active_month < choosen_month) {
    while (true) {
        index_active_month++

        Mobile.tap(findTestObject('Object Repository/Mobile/Datepicker/button_next_month'), 2)

        if (index_active_month == choosen_month) {
            break
        }
    }
}

Mobile.tap(findTestObject('Object Repository/Mobile/Datepicker/button_choosen_day'), 2)

Mobile.tap(findTestObject('Object Repository/Mobile/Datepicker/button_OK'), 2)

Mobile.tap(findTestObject('Object Repository/Mobile/Profile Page/input_profile_picture'), 2)

Mobile.tap(findTestObject('Object Repository/Mobile/Profile Page/button_take_from_camera'), 2)

Mobile.tap(findTestObject('Object Repository/Mobile/Profile Page/button_capture_camera'), 2)

Mobile.delay(2)

Mobile.tap(findTestObject('Object Repository/Mobile/Profile Page/button_capture_camera_okk'), 2)

Mobile.delay(2)

Mobile.tap(findTestObject('Object Repository/Mobile/Profile Page/button_capture_camera_savee'), 2)

Mobile.delay(2)

CustomKeywords.'com.helper.mobile.Screenshot.getSS'('1')

Mobile.tap(findTestObject('Object Repository/Mobile/Profile Page/button_save_changes'), 2)

if (Mobile.verifyElementExist(findTestObject('Object Repository/Mobile/Profile Page/span_success'), 2)) {
    assert true

    Mobile.comment('Notifikasi sukses')

    Mobile.delay(2)

    CustomKeywords.'com.helper.mobile.Screenshot.getSS'('2')

    Mobile.tap(findTestObject('Object Repository/Mobile/Profile Page/button_success_not_yet'), 2)
} else {
    assert false
}

