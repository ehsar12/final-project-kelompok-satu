import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

not_run: Mobile.callTestCase(findTestCase('Mobile/Change Profile/TCP_Login'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.comment(description)

Mobile.setText(findTestObject('Object Repository/Mobile/Profile Page/input_fullname'), fullname, 2)

Mobile.setText(findTestObject('Object Repository/Mobile/Profile Page/input_phone'), phone, 2)

Mobile.tap(findTestObject('Object Repository/Mobile/Profile Page/input_birthdate'), 2)

if (datebirth.equals('')) {
    Mobile.tap(findTestObject('Object Repository/Mobile/Datepicker/button_cancel'), 2)
} else {
    String[] date = datebirth.split('-')

    GlobalVariable.day_datepicker = (date[0])

    String month = date[1]

    int year = Integer.parseInt(date[2])

    int active_year = Integer.parseInt(Mobile.getAttribute(findTestObject('Object Repository/Mobile/Datepicker/span_active_year'), 
            'text', 2))

    String active_date = Mobile.getAttribute(findTestObject('Object Repository/Mobile/Datepicker/span_active_fulldate'), 
        'text', 2)

    String[] date_2 = active_date.split(' ')

    String month_2 = date_2[2]

    if (year < active_year) {
        Mobile.tap(findTestObject('Object Repository/Mobile/Datepicker/span_active_year'), 2)

        int decrease = active_year

        while (true) {
            Mobile.scrollToText(String.valueOf(decrease))

            decrease--

            if (decrease == year) {
                GlobalVariable.year_datepicker = year

                Mobile.tap(findTestObject('Object Repository/Mobile/Datepicker/button_choosen_year'), 2)

                break
            }
        }
    } else if (year > active_year) {
        Mobile.tap(findTestObject('Object Repository/Mobile/Datepicker/span_active_year'), 2)

        int increase = active_year++

        while (true) {
            Mobile.scrollToText(String.valueOf(increase))

            increase++

            if (increase == year) {
                GlobalVariable.year_datepicker = year

                Mobile.tap(findTestObject('Object Repository/Mobile/Datepicker/button_choosen_year'), 2)

                break
            }
        }
    }
    
    def choosen_month = GlobalVariable.month_list.findIndexOf({ 
            it == month
        })

    def active_month = GlobalVariable.month_list.findIndexOf({ 
            it == month_2
        })

    int index_active_month = active_month

    if (active_month > choosen_month) {
        while (true) {
            index_active_month--

            Mobile.tap(findTestObject('Object Repository/Mobile/Datepicker/button_prev_month'), 2)

            if (index_active_month == choosen_month) {
                break
            }
        }
    } else if (active_month < choosen_month) {
        while (true) {
            index_active_month++

            Mobile.tap(findTestObject('Object Repository/Mobile/Datepicker/button_next_month'), 2)

            if (index_active_month == choosen_month) {
                break
            }
        }
    }
    
    Mobile.tap(findTestObject('Object Repository/Mobile/Datepicker/button_choosen_day'), 2)

    Mobile.tap(findTestObject('Object Repository/Mobile/Datepicker/button_OK'), 2)
}

if (ID == 'MC-001') {
		Mobile.verifyElementExist(findTestObject('Object Repository/Mobile/Profile Page/span_incorrect_fullname'), 2)
	
		Mobile.verifyElementExist(findTestObject('Object Repository/Mobile/Profile Page/span_whatsapp_number_is_required'), 2)
	
		Mobile.verifyElementExist(findTestObject('Object Repository/Mobile/Profile Page/span_birthdate_cant_be_empty'), 2)
	
		Mobile.delay(2)
	
		CustomKeywords.'com.helper.mobile.Screenshot.getSS'(description)
} else if (ID == 'MC-002') {
	Mobile.verifyElementExist(findTestObject('Object Repository/Mobile/Profile Page/span_incorrect_fullname'), 2)

	Mobile.delay(2)

	CustomKeywords.'com.helper.mobile.Screenshot.getSS'(description)
} else if (ID == 'MC-003') {
	Mobile.verifyElementExist(findTestObject('Object Repository/Mobile/Profile Page/span_whatsapp_must_be_numeric'), 2)

	Mobile.delay(2)

	CustomKeywords.'com.helper.mobile.Screenshot.getSS'(description)
} else if (ID == 'MC-004') {
	Mobile.setText(findTestObject('Object Repository/Mobile/Profile Page/input_phone'), '62812345678912', 2)
	Mobile.verifyElementExist(findTestObject('Object Repository/Mobile/Profile Page/span_whatsapp_number_consists_of_9_13_characters'), 2)

	Mobile.delay(2)

	CustomKeywords.'com.helper.mobile.Screenshot.getSS'(description)
} else if (ID == 'MC-005') {
	Mobile.verifyElementExist(findTestObject('Object Repository/Mobile/Profile Page/span_incorrect_fullname'), 2)

	Mobile.delay(2)

	CustomKeywords.'com.helper.mobile.Screenshot.getSS'(description)
} else if (ID == 'MC-006') {
	Mobile.verifyElementExist(findTestObject('Object Repository/Mobile/Profile Page/span_whatsapp_number_is_required'), 2)

	Mobile.delay(2)

	CustomKeywords.'com.helper.mobile.Screenshot.getSS'(description)
} else if (ID == 'MC-007') {
	Mobile.verifyElementExist(findTestObject('Object Repository/Mobile/Profile Page/span_birthdate_cant_be_empty'), 2)

	Mobile.delay(2)

	CustomKeywords.'com.helper.mobile.Screenshot.getSS'(description)
} else {
	assert false
}

