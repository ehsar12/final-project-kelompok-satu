import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory as MobileDriverFactory
import io.appium.java_client.AppiumDriver as AppiumDriver

Mobile.startExistingApplication(GlobalVariable.appID, FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('Mobile/Beranda/txt_Welcome'), GlobalVariable.longTimeOut)

Mobile.verifyElementVisible(findTestObject('Mobile/Beranda/txt_Welcome'), 0)

CustomKeywords.'com.helper.mobile.Screenshot.getSS'('1')

Mobile.tap(findTestObject('Mobile/Beranda/btn_Profile'), 0)

Mobile.waitForElementPresent(findTestObject('Mobile/Profile Page/txt_Profile'), 0)

Mobile.verifyElementVisible(findTestObject('Mobile/Profile Page/txt_Profile'), 0)

CustomKeywords.'com.helper.mobile.Screenshot.getSS'('2')

Mobile.tap(findTestObject('Mobile/Profile Page/btn_Login Here'), 0)

Mobile.waitForElementPresent(findTestObject('Mobile/Login Page/txt_SignIn'), 0)

Mobile.verifyElementVisible(findTestObject('Mobile/Login Page/txt_SignIn'), 0)

CustomKeywords.'com.helper.mobile.Screenshot.getSS'('3')

driver = MobileDriverFactory.getDriver()

driver.terminateApp(GlobalVariable.appID)

