import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory as MobileDriverFactory
import io.appium.java_client.AppiumDriver as AppiumDriver

Mobile.callTestCase(findTestCase('Mobile/PreCondition/Login'), [:], FailureHandling.STOP_ON_FAILURE)

CustomKeywords.'com.helper.mobile.Screenshot.getSS'('1')

Mobile.setText(findTestObject('Mobile/Login Page/input_Email (1)'), email, 0)

Mobile.setText(findTestObject('Mobile/Login Page/input_Password (1)'), password, 0)

CustomKeywords.'com.helper.mobile.Screenshot.getSS'('2')

Mobile.tap(findTestObject('Mobile/Login Page/btn_HideShow'), 0)

Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)

CustomKeywords.'com.helper.mobile.Screenshot.getSS'('3')

Mobile.tap(findTestObject('Mobile/Login Page/btn_HideShow'), 0)

Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)

CustomKeywords.'com.helper.mobile.Screenshot.getSS'('4')

String passwordType = Mobile.getAttribute(findTestObject('Mobile/Login Page/input_Password (1)'), 'password', 0)

if (passwordType == 'true') {
    println('Password is hidden')

    assert true
} else {
    println('Password is shown')

    assert false
}

driver = MobileDriverFactory.getDriver()

driver.terminateApp(GlobalVariable.appID)

