import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory as MobileDriverFactory
import io.appium.java_client.AppiumDriver as AppiumDriver

'Memulai aplikasi yang sedang berjalan dengan ID yang diberikan oleh variabel GlobalVariable.appID dan menghentikan eksekusi script jika terjadi kegagalan.'
Mobile.startExistingApplication(GlobalVariable.appID, FailureHandling.STOP_ON_FAILURE)

'Menunggu element dengan nama \'Mobile/Beranda/txt_Welcome\' muncul selama waktu yang diberikan oleh variabel GlobalVariable.longTimeOut.'
Mobile.waitForElementPresent(findTestObject('Mobile/Beranda/txt_Welcome'), GlobalVariable.longTimeOut)

'Memverifikasi bahwa element dengan nama \'Mobile/Beranda/txt_Welcome\' terlihat.'
Mobile.verifyElementVisible(findTestObject('Mobile/Beranda/txt_Welcome'), 0)

'Mengambil screenshot saat ini dan memberinya nama \'1\'.'
CustomKeywords.'com.helper.mobile.Screenshot.getSS'('1')

'Tap tombol dengan nama \'Mobile/Beranda/btn_Login Here\'.'
Mobile.tap(findTestObject('Mobile/Beranda/btn_Login Here'), 0)

'Menunggu element dengan nama \'Mobile/Login Page/txt_SignIn\' muncul.'
Mobile.waitForElementPresent(findTestObject('Mobile/Login Page/txt_SignIn'), 0)

'Memverifikasi bahwa element dengan nama \'Mobile/Login Page/txt_SignIn\' terlihat.'
Mobile.verifyElementVisible(findTestObject('Mobile/Login Page/txt_SignIn'), 0)

'Mengambil screenshot saat ini dan memberinya nama \'2\'.'
CustomKeywords.'com.helper.mobile.Screenshot.getSS'('2')

'Menyimpan driver yang digunakan saat ini ke dalam variabel driver.'
driver = MobileDriverFactory.getDriver()

'Menutup aplikasi dengan ID yang diberikan oleh variabel GlobalVariable.appID.'
driver.terminateApp(GlobalVariable.appID)

