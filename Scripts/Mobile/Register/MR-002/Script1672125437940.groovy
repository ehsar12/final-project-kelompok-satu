import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory as MobileDriverFactory
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import internal.GlobalVariable as GlobalVariable

'Precondition'
Mobile.comment('Pre-condition')

'Memulai aplikasi'
Mobile.startExistingApplication('com.codingid.codingidhive.betastaging', FailureHandling.STOP_ON_FAILURE)

'Menunggu hingga element Text View tampil'
Mobile.waitForElementPresent(findTestObject('Mobile/Main Page/tv_welcome'), 10)

'Berada pada halaman utama'
CustomKeywords.'com.helper.mobile.Screenshot.getSS'('1')

'Steps'
Mobile.comment('Steps')

'Memastikan bahwa elemen button profile tampil'
Mobile.verifyElementExist(findTestObject('Mobile/Navbar/button_profile'), 1)

'Tap button Profile'
Mobile.tap(findTestObject('Mobile/Navbar/button_profile'), 1)

'Memastikan bahwa elemen header profile tampil'
Mobile.verifyElementVisible(findTestObject('Mobile/Profile Page/header_profile'), 1)

'Berada pada halaman profile'
CustomKeywords.'com.helper.mobile.Screenshot.getSS'('2')

'Tap button login here'
Mobile.tap(findTestObject('Mobile/Profile Page/btn_login_here'), 1)

'Memastikan bahwa button header sign in tampil'
Mobile.verifyElementVisible(findTestObject('Mobile/Login Page/tv_header_signin'), 0)

'Berada pada halaman sign in'
CustomKeywords.'com.helper.mobile.Screenshot.getSS'('3')

'Tap button Register Now'
Mobile.tap(findTestObject('Mobile/Login Page/btn_register_now'), 0)

'Validation'
Mobile.comment('Validation')

'Memastikan bahwa elemen header register tampil'
Mobile.verifyElementVisible(findTestObject('Mobile/Register Page/header_register'), 1)

'Berada pada halaman register'
CustomKeywords.'com.helper.mobile.Screenshot.getSS'('4')

driver = MobileDriverFactory.getDriver()

driver.terminateApp(GlobalVariable.appID)

