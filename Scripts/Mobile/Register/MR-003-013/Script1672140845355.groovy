import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory as MobileDriverFactory
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint

'Testcase ID'
Mobile.comment('TESTCASE: ' + ID + ' - ' + scenario)

'Pre-condition'
Mobile.comment('Pre-condition')

'Memulai aplikasi'
Mobile.startExistingApplication(GlobalVariable.appID, FailureHandling.STOP_ON_FAILURE)

'Menunggu hingga element text view welcome tampil'
Mobile.waitForElementPresent(findTestObject('Mobile/Main Page/tv_welcome'), 10)

'Tap element button login here'
Mobile.tap(findTestObject('Mobile/Main Page/btn_login_here'), 1)

'Tap element button Register Now'
Mobile.tap(findTestObject('Mobile/Login Page/btn_register_now'), 1)

'Memastikan element header register tampil'
Mobile.verifyElementVisible(findTestObject('Mobile/Register Page/header_register'), 2)

'Berada pada halaman register'
CustomKeywords.'com.helper.mobile.Screenshot.getSS'('1')

'Steps'
Mobile.comment('Steps')

'Input nama pada field name'
Mobile.setText(findTestObject('Mobile/Register Page/et_name'), name, 1)

if (!(datebirth.equals(''))) {
    'Tap field birthdate'
    Mobile.tap(findTestObject('Mobile/Register Page/et_birthdate'), 1)

    String[] date = datebirth.split('-')

    GlobalVariable.day_datepicker = (date[0])

    String month = date[1]

    int year = Integer.parseInt(date[2])

    int active_year = Integer.parseInt(Mobile.getAttribute(findTestObject('Object Repository/Mobile/Register Page/DatePicker/button_year'), 
            'text', 2))

    String active_date = Mobile.getAttribute(findTestObject('Object Repository/Mobile/Register Page/DatePicker/tv_day_date'), 
        'text', 2)

    String[] date_2 = active_date.split(' ')

    String month_2 = date_2[2]

    if (year < active_year) {
        'Tap opsi tahun'
        Mobile.tap(findTestObject('Object Repository/Mobile/Register Page/DatePicker/button_year'), 2)

        int decrease = active_year

        while (true) {
            'Scroll hingga teks tampil'
            Mobile.scrollToText(String.valueOf(decrease))

            decrease--

            if (decrease == year) {
                GlobalVariable.year_datepicker = year

                'Tap tahun pilihan'
                Mobile.tap(findTestObject('Object Repository/Mobile/Register Page/DatePicker/button_choosen_year'), 2)

                break
            }
        }
    } else if (year > active_year) {
        'Tap opsi tahun'
        Mobile.tap(findTestObject('Object Repository/Mobile/Register Page/DatePicker/button_year'), 2)

        int increase = active_year++

        while (true) {
            'Scroll hingga teks tampil'
            Mobile.scrollToText(String.valueOf(increase))

            increase++

            if (increase == year) {
                GlobalVariable.year_datepicker = year

                'Tap tahun pilihan'
                Mobile.tap(findTestObject('Object Repository/Mobile/Register Page/DatePicker/button_choosen_year'), 2)

                break
            }
        }
    }
    
    def choosen_month = GlobalVariable.month_list.findIndexOf({ 
            it == month
        })

    def active_month = GlobalVariable.month_list.findIndexOf({ 
            it == month_2
        })

    int index_active_month = active_month

    if (active_month > choosen_month) {
        while (true) {
            index_active_month--

            'Tap button prev'
            Mobile.tap(findTestObject('Object Repository/Mobile/Register Page/DatePicker/btn_prev'), 2)

            if (index_active_month == choosen_month) {
                break
            }
        }
    } else if (active_month < choosen_month) {
        while (true) {
            index_active_month++

            'Tap button next'
            Mobile.tap(findTestObject('Object Repository/Mobile/Register Page/DatePicker/btn_next'), 2)

            if (index_active_month == choosen_month) {
                break
            }
        }
    }
    
    'Tap tanggal pilihan'
    Mobile.tap(findTestObject('Object Repository/Mobile/Register Page/DatePicker/button_choosen_day'), 2)

    'Tap button Oke'
    Mobile.tap(findTestObject('Object Repository/Mobile/Register Page/DatePicker/button_oke'), 2)
}

'Input email pada field email'
Mobile.setText(findTestObject('Mobile/Register Page/et_email'), email, 1)

'Input whatsapp pada field whatsapp'
Mobile.setText(findTestObject('Mobile/Register Page/et_whatsapp'), whatsapp, 1)

'Input password pada field password'
Mobile.setText(findTestObject('Mobile/Register Page/et_password'), password, 1)

'Input confirm password pada field confirm password'
Mobile.setText(findTestObject('Mobile/Register Page/et_confirm_password'), confirm_password, 1)

if (ID == '010') {
    'Hapus field nama'
    Mobile.setText(findTestObject('Mobile/Register Page/et_name'), '', 1)
} else if (ID == '011') {
    'Tap field birthdate'
    Mobile.tap(findTestObject('Mobile/Register Page/et_birthdate'), 1)

    'Tap button batal'
    Mobile.tap(findTestObject('Mobile/Register Page/DatePicker/button_batal'), 1)
} else if (ID == '012') {
    'Hapus field email'
    Mobile.setText(findTestObject('Mobile/Register Page/et_email'), '', 1)
} else if (ID == '013') {
    'Hapus field whatsapp'
    Mobile.setText(findTestObject('Mobile/Register Page/et_whatsapp'), '', 1)
}

'Field berhasil di isi'
CustomKeywords.'com.helper.mobile.Screenshot.getSS'('2')

'Centang term and condition'
Mobile.checkElement(findTestObject('Mobile/Register Page/cb_term'), 1)

'Validation'
Mobile.comment('Validation')

if (ID == '003') {
    'Memastikan button daftar tidak dapat di tap'
    Mobile.verifyElementAttributeValue(findTestObject('Mobile/Register Page/btn_daftar'), 'Clickable', 'false', 2, FailureHandling.CONTINUE_ON_FAILURE)

    'Button tidak dapat di tap'
    CustomKeywords.'com.helper.mobile.Screenshot.getSS'('3')
} else if (((((ID == '004') | (ID == '005')) | (ID == '006')) | (ID == '008')) | (ID == '009')) {
    'Memastikan element alert tampil'
    Mobile.verifyElementExist(findTestObject('Mobile/Register Page/alert/alert_message', [('alert') : invalid_message]), 
        2, FailureHandling.CONTINUE_ON_FAILURE)

    'Memastikan button daftar tidak dapat ditekan'
    Mobile.verifyElementAttributeValue(findTestObject('Mobile/Register Page/btn_daftar'), 'Clickable', 'false', 2, FailureHandling.CONTINUE_ON_FAILURE)

    'Button tidak dapat ditekan dan tidak dapat melanjutkan register'
    CustomKeywords.'com.helper.mobile.Screenshot.getSS'('3')
} else if ((((ID == '010') | (ID == '011')) | (ID == '012')) | (ID == '013')) {
    'Memastikan element alert tampil'
    Mobile.verifyElementExist(findTestObject('Mobile/Register Page/alert/alert_message', [('alert') : empty_message]), 2, 
        FailureHandling.CONTINUE_ON_FAILURE)

    'Memastikan button daftar tidak dapat ditekan'
    Mobile.verifyElementAttributeValue(findTestObject('Mobile/Register Page/btn_daftar'), 'Clickable', 'false', 2)

    'Button tidak dapat ditekan dan tidak dapat melanjutkan register'
    CustomKeywords.'com.helper.mobile.Screenshot.getSS'('3')
} else {
    assert false
}

driver = MobileDriverFactory.getDriver()

driver.terminateApp(GlobalVariable.appID)

