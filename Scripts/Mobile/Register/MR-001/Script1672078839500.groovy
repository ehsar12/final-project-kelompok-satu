import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory as MobileDriverFactory
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

'Precondition'
Mobile.comment('Pre-condition')

'Memulai aplikasi'
Mobile.startExistingApplication(GlobalVariable.appID, FailureHandling.STOP_ON_FAILURE)

'Menunggu hingga element Text View tampil'
Mobile.waitForElementPresent(findTestObject('Mobile/Main Page/tv_welcome'), 10)

'Berada pada halaman utama'
CustomKeywords.'com.helper.mobile.Screenshot.getSS'('1')

'Steps'
Mobile.comment('Steps')

'Memastikan element button login here ada'
Mobile.verifyElementExist(findTestObject('Mobile/Main Page/btn_login_here'), 1)

'Tap button login here'
Mobile.tap(findTestObject('Mobile/Main Page/btn_login_here'), 1)

'Memastikan element header sign in tampil'
Mobile.verifyElementVisible(findTestObject('Mobile/Login Page/tv_header_signin'), 1)

'Berada pada halaman sign in'
CustomKeywords.'com.helper.mobile.Screenshot.getSS'('2')

'Tap button register now'
Mobile.tap(findTestObject('Mobile/Login Page/btn_register_now'), 1)

'Validation'
Mobile.comment('Validation')

'Memastikan element header register tampil'
Mobile.verifyElementVisible(findTestObject('Mobile/Register Page/header_register'), 1)

'Berada pada halaman register'
CustomKeywords.'com.helper.mobile.Screenshot.getSS'('3')

driver = MobileDriverFactory.getDriver()

driver.terminateApp(GlobalVariable.appID)

