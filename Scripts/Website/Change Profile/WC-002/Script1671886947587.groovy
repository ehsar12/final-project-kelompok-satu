import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

'Jalankan test case Login dengan parameter email, password, dan url'
WebUI.callTestCase(findTestCase('Website/Pre-condition/Login'), [('email') : 'qn764v+epprfoaq42jg8@sharklasers.com', ('password') : 'katasandi123'
        , ('url') : GlobalVariable.url], FailureHandling.STOP_ON_FAILURE)

'Ambil screenshot halaman'
CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'('1')

'Buka halaman profile edit'
WebUI.navigateToUrl(GlobalVariable.url + 'dashboard/profile/edit')

'Tunggu element input_A_photo muncul di halaman'
WebUI.waitForElementPresent(findTestObject('Website/Change Profile Page/input_A_photo'), 0)

'Verifikasi apakah element input_A_photo muncul di halaman'
WebUI.verifyElementPresent(findTestObject('Website/Change Profile Page/input_A_photo'), 0)

'Ambil screenshot halaman'
CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'('2')

'Tutup browser'
WebUI.closeBrowser()

