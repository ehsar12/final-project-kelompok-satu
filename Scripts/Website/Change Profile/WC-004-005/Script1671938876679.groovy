import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

'Jalankan test case Change Profile Page'
WebUI.callTestCase(findTestCase('Website/Pre-condition/Change Profile Page'), [:], FailureHandling.STOP_ON_FAILURE)

'Ambil screenshot halaman'
CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'(filename + '-1')

'Dapatkan direktori file yang sedang berjalan'
BaseFileDir = System.getProperty('user.dir')

'Dapatkan objek input untuk meng-upload file'
UploadObj = findTestObject('Website/Change Profile Page/input_A_photo')

'Dapatkan lokasi file yang akan di-upload'
FilePath = (BaseFileDir + '/Data Files/Website/Change Profile/')

'Tunggu element input_A_photo muncul di halaman'
WebUI.waitForElementPresent(findTestObject('Website/Change Profile Page/input_A_photo'), 0)

'Verifikasi apakah element input_A_photo muncul di halaman'
WebUI.verifyElementPresent(findTestObject('Website/Change Profile Page/input_A_photo'), 0)

'Ambil screenshot halaman'
CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'(filename + '-2')

'Klik icon profile dan pilih file yang akan di-upload'
WebUI.uploadFile(UploadObj, FilePath + filename)

'Ambil screenshot halaman'
CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'(filename + '-3')

'Klik tombol Save Changes untuk menyimpan perubahan'
WebUI.click(findTestObject('Website/Change Profile Page/button_Save Changes'))

'Tunggu element txt_Berhasil muncul di halaman'
WebUI.waitForElementPresent(findTestObject('Website/Profile Page/txt_Berhasil'), 5)

'Verifikasi apakah perubahan profile berhasil disimpan'
WebUI.verifyElementPresent(findTestObject('Website/Profile Page/txt_Berhasil'), 5)

'Ambil screenshot halaman'
CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'(filename + '-4')

'Klik tombol OK pada notifikasi berhasil'
WebUI.click(findTestObject('Website/Profile Page/button_Success_OK'))

'Tunggu 3 detik'
WebUI.delay(3)

'Ambil screenshot halaman'
CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'(filename + '-5')

'Tutup browser'
WebUI.closeBrowser()

