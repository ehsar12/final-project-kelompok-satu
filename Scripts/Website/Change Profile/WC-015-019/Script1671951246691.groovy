import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

'Panggil test case Website/Pre-condition/Change Profile Page dan hentikan jika terjadi kegagalan'
WebUI.callTestCase(findTestCase('Website/Pre-condition/Change Profile Page'), [:], FailureHandling.STOP_ON_FAILURE)

'Ambil screenshot web saat ini'
CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'(testcase + '-1')

'Set nilai input_Phone_whatsapp di halaman Change Profile Page dengan phone_number'
WebUI.setText(findTestObject('Website/Change Profile Page/input_Phone_whatsapp'), phone_number)

'Ambil screenshot web saat ini'
CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'(testcase + '-2')

'Klik tombol Save Changes di halaman Change Profile Page'
WebUI.click(findTestObject('Website/Change Profile Page/button_Save Changes'))

'Jika testcase bernilai \'015\', tunggu sampai elemen errorTxt_The whatsapp field is required muncul, verifikasi keberadaannya, ambil screenshot web saat ini, dan pastikan benar\nJika testcase bernilai \'016\', tunggu sampai elemen errorTxt_The whatsapp must be a number muncul, verifikasi keberadaannya, ambil screenshot web saat ini, dan pastikan benar\nJika testcase bernilai \'017\' atau \'018\', tunggu sampai elemen errorTxt_The whatsapp must be between 10 and 12 digits muncul, verifikasi keberadaannya, ambil screenshot web saat ini, dan pastikan benar\nJika testcase bernilai selain \'015\', \'016\', \'017\', atau \'018\', ambil screenshot web saat ini, dan pastikan salah'
if (testcase == '015') {
    WebUI.waitForElementPresent(findTestObject('Website/Change Profile Page/errorTxt_The whatsapp field is required'), 3)

    WebUI.verifyElementPresent(findTestObject('Website/Change Profile Page/errorTxt_The whatsapp field is required'), 3)

    CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'(testcase + '-3')

    assert true
} else if (testcase == '016') {
    WebUI.waitForElementPresent(findTestObject('Website/Change Profile Page/errorTxt_The whatsapp must be a number'), 3)

    WebUI.verifyElementPresent(findTestObject('Website/Change Profile Page/errorTxt_The whatsapp must be a number'), 3)

    CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'(testcase + '-3')

    assert true

} else if ((testcase == '017') || (testcase == '018')) {
    WebUI.waitForElementPresent(findTestObject('Website/Change Profile Page/errorTxt_The whatsapp must be between 10 and 12 digits'), 
        3)

    WebUI.verifyElementPresent(findTestObject('Website/Change Profile Page/errorTxt_The whatsapp must be between 10 and 12 digits'), 
        3)

    CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'(testcase + '-3')

    assert true

} else {
    CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'(testcase + '-3')

    assert false
}

'Tutup browser web saat ini'
WebUI.closeBrowser()

