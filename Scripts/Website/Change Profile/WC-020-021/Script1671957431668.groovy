import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

'Memanggil test case dengan nama \'Website/Pre-condition/Change Profile Page\' dan menghentikan eksekusi script jika terjadi kegagalan.'
WebUI.callTestCase(findTestCase('Website/Pre-condition/Change Profile Page'), [:], FailureHandling.STOP_ON_FAILURE)

'Mengambil screenshot saat ini dan memberinya nama yang disediakan oleh variabel phone_number + \'-1\'.'
CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'(phone_number + '-1')

'Mengisi kolom teks dengan nomor telepon yang disediakan oleh variabel phone_number.'
WebUI.setText(findTestObject('Website/Change Profile Page/input_Phone_whatsapp'), phone_number)

'Mengambil screenshot saat ini dan memberinya nama yang disediakan oleh variabel phone_number + \'-2\'.'
CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'(phone_number + '-2')

'Menekan tombol dengan nama \'button_Save Changes\'.'
WebUI.click(findTestObject('Website/Change Profile Page/button_Save Changes'))

'Menunggu element dengan nama \'Website/Profile Page/txt_Berhasil\' muncul selama 3 detik.'
WebUI.waitForElementPresent(findTestObject('Website/Profile Page/txt_Berhasil'), 3)

'Memverifikasi bahwa element dengan nama \'Website/Profile Page/txt_Berhasil\' muncul selama 3 detik.'
WebUI.verifyElementPresent(findTestObject('Website/Profile Page/txt_Berhasil'), 3)

'Mengambil screenshot saat ini dan memberinya nama yang disediakan oleh variabel phone_number + \'-4\'.'
CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'(phone_number + '-4')

'Menekan tombol dengan nama \'button_Success_OK\'.'
WebUI.click(findTestObject('Website/Profile Page/button_Success_OK'))

'Mengambil screenshot saat ini dan memberinya nama yang disediakan oleh variabel phone_number + \'-5\'.'
CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'(phone_number + '-5')

'Menutup browser web saat ini.'
WebUI.closeBrowser()

