import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

'Buka browser dan arahkan ke halaman profile edit'
WebUI.openBrowser(GlobalVariable.url + 'dashboard/profile/edit')

'Atur ukuran viewport browser sesuai dengan variable sreenWidth dan screenHeight'
WebUI.setViewPortSize(GlobalVariable.sreenWidth, GlobalVariable.screenHeight)

'Tunggu element txt_Masuk muncul di halaman'
WebUI.waitForElementPresent(findTestObject('Website/Login Page/txt_Masuk'), 0)

'Tunggu element txt_Masuk muncul di halaman'
WebUI.verifyElementPresent(findTestObject('Website/Login Page/txt_Masuk'), 0)

'Ambil screenshot halaman'
CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'('1')

'Tutup browser'
WebUI.closeBrowser()

