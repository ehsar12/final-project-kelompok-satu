import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

'Panggil test case Website/Pre-condition/Change Profile Page dan hentikan jika terjadi kegagalan'
WebUI.callTestCase(findTestCase('Website/Pre-condition/Change Profile Page'), [:], FailureHandling.STOP_ON_FAILURE)

'Ambil screenshot halaman saat ini'
CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'('1')

'Mencoba mengubah teks pada element dengan objek test case "Website/Login Page/input_email" dengan nilai variabel "email". Jika berhasil, mengambil screenshot dengan memanggil custom keyword "com.helper.website.ScreenshotWeb.getSS" dengan parameter "berhasil ubah email", kemudian menutup browser. Jika gagal, mengambil screenshot dengan memanggil custom keyword "com.helper.website.ScreenshotWeb.getSS" dengan parameter "gagal ubah email", kemudian menutup browser.'
try {
    WebUI.setText(findTestObject('Website/Login Page/input_email'), email)

    CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'('berhasil ubah email')

    WebUI.click(findTestObject('Website/Change Profile Page/button_Save Changes'))

    WebUI.closeBrowser()

    assert false
}
catch (Exception e) {
    CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'('gagal ubah email')

    WebUI.closeBrowser()

    assert true
} 

