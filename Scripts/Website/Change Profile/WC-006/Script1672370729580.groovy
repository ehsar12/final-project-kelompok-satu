import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

'Memanggil test case dengan nama "Website/Pre-condition/Change Profile Page" dan menangani kegagalan dengan menghentikan eksekusi script ketika terjadi kegagalan.'
WebUI.callTestCase(findTestCase('Website/Pre-condition/Change Profile Page'), [:], FailureHandling.STOP_ON_FAILURE)

'Memanggil custom keyword bernama "com.helper.website.ScreenshotWeb.getSS" dengan parameter "1".'
CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'('1')

'Menyimpan lokasi direktori file saat ini ke dalam variabel "BaseFileDir".'
BaseFileDir = System.getProperty('user.dir')

'Menyimpan objek test case dengan nama "Website/Change Profile Page/input_A_photo" ke dalam variabel "UploadObj".'
UploadObj = findTestObject('Website/Change Profile Page/input_A_photo')

'Menyimpan lokasi direktori file untuk data website "Change Profile" ke dalam variabel "FilePath".'
FilePath = (BaseFileDir + '/Data Files/Website/Change Profile/')

'Menunggu element dengan objek test case "Website/Change Profile Page/input_A_photo" muncul pada halaman web'
WebUI.waitForElementPresent(findTestObject('Website/Change Profile Page/input_A_photo'), 0)

'Memverifikasi apakah element dengan objek test case "Website/Change Profile Page/input_A_photo" muncul pada halaman web '
WebUI.verifyElementPresent(findTestObject('Website/Change Profile Page/input_A_photo'), 0)

'Memanggil custom keyword bernama "com.helper.website.ScreenshotWeb.getSS" dengan parameter "2".'
CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'('2')

'Menyimpan file yang akan diunggah ke dalam variabel "UploadFile".'
UploadFile = ((((FilePath + filename1) + '\n') + FilePath) + filename2)

'Mencoba mengunggah file ke element dengan objek test case "Website/Change Profile Page/input_A_photo". Jika berhasil, mengambil screenshot dengan memanggil custom keyword "com.helper.website.ScreenshotWeb.getSS" dengan parameter "berhasil-upload", kemudian menutup browser. Jika gagal, mengambil screenshot dengan memanggil custom keyword "com.helper.website.ScreenshotWeb.getSS" dengan parameter "gagal-upload", kemudian menutup browser.'
try {
    WebUI.uploadFile(UploadObj, UploadFile)

    CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'('berhasil-upload')

    WebUI.closeBrowser()

    assert false
}
catch (Exception e) {
    CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'('gagal-upload')

    WebUI.closeBrowser()

    assert true
} 

