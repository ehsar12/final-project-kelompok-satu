import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

'Jalankan test case Change Profile Page'
WebUI.callTestCase(findTestCase('Website/Pre-condition/Change Profile Page'), [:], FailureHandling.STOP_ON_FAILURE)

'Ambil screenshot halaman'
CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'(fullname + '-1')

'Input nama lengkap baru pada kolom Fullname'
WebUI.setText(findTestObject('Website/Change Profile Page/input_Fullname_name'), fullname)

'Ambil screenshot halaman'
CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'(fullname + '-2')

'Klik tombol Save Changes untuk menyimpan perubahan nama lengkap'
WebUI.click(findTestObject('Website/Change Profile Page/button_Save Changes'))

'Tunggu element errorTxt_The name field is required muncul di halaman'
WebUI.waitForElementPresent(findTestObject('Website/Change Profile Page/errorTxt_The name field is required'), 3, FailureHandling.STOP_ON_FAILURE)

'Verifikasi apakah perubahan nama lengkap berhasil disimpan'
WebUI.verifyElementPresent(findTestObject('Website/Change Profile Page/errorTxt_The name field is required'), 3, FailureHandling.STOP_ON_FAILURE)

'Ambil screenshot halaman'
CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'(fullname + '-3')

'Tutup browser'
WebUI.closeBrowser()

