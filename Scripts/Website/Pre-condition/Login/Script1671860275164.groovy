import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.core.context.TestCaseContext as TestCaseContext
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.webui.keyword.builtin.TakeScreenshotKeyword as TakeScreenshotKeyword
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

'Redirect ke halaman Login'
WebUI.openBrowser(GlobalVariable.url + 'login')

WebUI.setViewPortSize(GlobalVariable.sreenWidth, GlobalVariable.screenHeight)

WebUI.waitForElementPresent(findTestObject('Website/Login Page/txt_Masuk'), 2)

WebUI.verifyElementPresent(findTestObject('Website/Login Page/txt_Masuk'), 0)

'Masukkan E-Mail'
WebUI.setText(findTestObject('Website/Login Page/input_email'), email)

'Masukkan Password'
WebUI.setText(findTestObject('Website/Login Page/input_password'), password)

'Klik button Login'
WebUI.click(findTestObject('Website/Login Page/button_login'))

WebUI.waitForElementPresent(findTestObject('Website/Home Page/menu_is_logged'), 1)

'Memastikan apakah sudah berhasil login dan diarahkan ke halaman beranda'
WebUI.verifyElementPresent(findTestObject('Website/Home Page/menu_is_logged'), 1)

