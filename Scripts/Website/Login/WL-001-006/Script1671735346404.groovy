import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.comment(description)

WebUI.openBrowser(GlobalVariable.url)

WebUI.maximizeWindow()

WebUI.click(findTestObject('Website/Home Page/menu_masuk'))

WebUI.setText(findTestObject('Website/Login Page/input_email'), email)

WebUI.setText(findTestObject('Website/Login Page/input_password'), password)

WebUI.click(findTestObject('Website/Login Page/button_login'))

if(ID == 'WL-001' || ID == 'WL-002' || ID == 'WL-003') {
	WebUI.verifyElementPresent(findTestObject('Object Repository/Website/Login Page/button_login'), 2)
	
	WebUI.delay(2)
	
	CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'(description)
} else if(ID == 'WL-004' || ID == 'WL-005' || ID == 'WL-006') {
	WebUI.verifyElementPresent(findTestObject('Object Repository/Website/Login Page/span_emall_atau_kata_sandi_salah'), 2)
	
	WebUI.delay(2)
	
	CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'(description)
} else {
	assert false
}

WebUI.closeBrowser()

