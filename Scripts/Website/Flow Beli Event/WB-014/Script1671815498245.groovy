import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

'Validation'
WebUI.comment('Pre-condition')

'Menjalankan Testcase Login'
WebUI.callTestCase(findTestCase('Website/Pre-condition/Login'), [('email') : findTestData('Website/Flow Beli Event/login').getValue(
            1, 2), ('password') : findTestData('Website/Flow Beli Event/login').getValue(2, 2), ('url') : GlobalVariable.url], 
    FailureHandling.STOP_ON_FAILURE)

'Memastikan berhasil login dan tampil header pada halaman utama'
WebUI.verifyElementPresent(findTestObject('Website/Home Page/header'), 1)

'Arahkan ke halaman detail event'
WebUI.navigateToUrl(GlobalVariable.detailEventPage1)

'Menjalankan Testcase Memasukkan event ke keranjang'
WebUI.callTestCase(findTestCase('Website/Flow Beli Event/WB-012'), [('event_name') : findTestData('Website/Flow Beli Event/list_event').getValue(
            1, 1)], FailureHandling.STOP_ON_FAILURE)

'Berada pada modal success dan tampil pesan add to cart success'
CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'('1')

'Steps'
WebUI.comment('Steps')

'Klik button "Lihat event lainnya"'
WebUI.click(findTestObject('Website/Modal Success/button_other_event'))

'Validation'
WebUI.comment('Validation')

'Memastikan header pada halaman events tampil'
WebUI.verifyElementPresent(findTestObject('Website/Event Page/header_menu_event'), 1)

'Berada pada halaman events'
CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'('2')

