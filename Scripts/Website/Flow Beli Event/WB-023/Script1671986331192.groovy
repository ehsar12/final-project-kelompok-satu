import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import internal.GlobalVariable as GlobalVariable

'Precondition'
WebUI.comment('Pre-condition')

List<WebElement> cboxes = WebUiCommonHelper.findWebElements(findTestObject('Website/Cart Page/checkbox'), 11)

for (WebElement cbox : cboxes) {
    attrName = cbox.getAttribute('name')

    WebUI.verifyElementNotChecked(findTestObject('Website/Cart Page/checkbox', [('event_name') : attrName]), 1)
}

'Berada pada halaman keranjang dan terdapat event'
CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'('1')

'Steps'
WebUI.comment('Steps')

'Klik button checkout'
WebUI.click(findTestObject('Website/Cart Page/button_checkout'))

'Validation'
WebUI.comment('Validation')

'Memastikan modal informasi tampil'
WebUI.verifyElementPresent(findTestObject('Website/Cart Page/Modal/Information/modal_info'), 1)

'Tampil modal informasi'
CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'('2')

'Klik button close modal'
WebUI.click(findTestObject('Website/Cart Page/Modal/Information/button_close_modal'))

