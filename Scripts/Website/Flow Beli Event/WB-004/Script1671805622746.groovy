import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

'Pre-Condition'
WebUI.comment('Pre-condition')

'Arahkan ke url'
WebUI.navigateToUrl(GlobalVariable.url)

'Memastikan header homepage tampil'
WebUI.verifyElementPresent(findTestObject('Website/Home Page/header'), 1)

'Berada pada halaman utama'
CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'('1')

'Steps'
WebUI.comment('Steps')

'Memastikan element Event dapat di klik'
WebUI.waitForElementClickable(findTestObject('Website/Home Page/list_event', [('event_name') : event_name]), 1)

'Klik event pada block online course'
WebUI.click(findTestObject('Website/Home Page/list_event', [('event_name') : event_name]))

'Validation'
WebUI.comment('Validation')

'Memastikan judul event telah tampil'
WebUI.verifyElementPresent(findTestObject('Website/Detail Event/event_title'), 1)

'Memasukkan url detail event pada global variabel'
GlobalVariable.detailEventPage1 = WebUI.getUrl()

'Berhasil diarahkan ke halaman detail event'
CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'('2')
