import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

'Precondition'
WebUI.comment('Pre-condition')

'Menjalankan testcase Login'
WebUI.callTestCase(findTestCase('Website/Pre-condition/Login'), [('email') : email, ('password') : password, ('url') : GlobalVariable.url], 
    FailureHandling.STOP_ON_FAILURE)

'Berhasil login dan berada pada mainpage'
CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'('1')

'Steps'
WebUI.comment('Steps')

'Klik menu Events pada Navigasi Bar'
WebUI.click(findTestObject('Website/Home Page/menu_events'))

'Menunggu element header menu event tampil'
WebUI.waitForElementVisible(findTestObject('Website/Event Page/header_menu_event'), 1)

'Menyimpan url halaman event ke global variabel'
GlobalVariable.eventPageUrl = WebUI.getUrl()

'Berhasil diarahkan ke halaman events'
CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'('2')

