import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By as By
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WE
import com.kms.katalon.core.webui.common.WebUiCommonHelper as helper
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory

'Precondition'
WebUI.comment('Pre-condition')

WebDriver driver = DriverFactory.getWebDriver()

TestObject cbox = findTestObject('Website/Cart Page/checkbox')

'Menghitung jumlah item pada keranjang'
def numElem1 = driver.findElements(By.xpath(cbox.findPropertyValue('xpath'))).size()

'Berada pada halaman Keranjang'
CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'('1')

'Steps'
WebUI.comment('Steps')

'Klik button profile'
WebUI.click(findTestObject('Website/Home Page/menu_is_logged'))

'Tampil popup menu'
CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'('2')

'Klik menu logout'
WebUI.click(findTestObject('Website/Home Page/menu_logout'))

'Berhasil logout dan berada pada halaman utama'
CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'('3')

'Menjalankan testcase login'
WebUI.callTestCase(findTestCase('Website/Pre-condition/Login'), [('email') : findTestData('Website/Flow Beli Event/login').getValue(
            1, 2), ('password') : findTestData('Website/Flow Beli Event/login').getValue(2, 2), ('url') : GlobalVariable.url], 
    FailureHandling.STOP_ON_FAILURE)

'Arahkan ke halaman keranjang'
WebUI.navigateToUrl(GlobalVariable.cartPage)

'Berhasil diarahkan ke halaman keranjang'
CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'('4')

'Validation'
WebUI.comment('Validation')

TestObject cbox2 = findTestObject('Website/Cart Page/checkbox')

WebDriver driver2 = DriverFactory.getWebDriver()

'Menghitung jumlah item pada keranjang'
def numElem2 = driver2.findElements(By.xpath(cbox2.findPropertyValue('xpath'))).size()

'Pastikan jumlah item saat ini sama dengan sebelumnya'
assert numElem2 == numElem1

'Jumlah item event sama dengan sebelumnya'
CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'('5')

