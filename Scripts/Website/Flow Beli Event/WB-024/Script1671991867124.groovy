import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import internal.GlobalVariable as GlobalVariable

'Precondition'
WebUI.comment('Pre-condition')

'Berada pada halaman keranjang'
CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'('1')

'Steps'
WebUI.comment('Steps')

List<WebElement> cboxes = WebUiCommonHelper.findWebElements(findTestObject('Website/Cart Page/checkbox'), 11)

for (WebElement cbox : cboxes) {
	'Klik checkbox pada masing-masing event'
    cbox.click()
}

'Klik button checkout'
WebUI.click(findTestObject('Website/Cart Page/button_checkout'))

'Memastikan popup modal detail pembayaran tampil'
WebUI.verifyElementPresent(findTestObject('Website/Cart Page/Modal/Detail Pembayaran/popup_modal'), 1)

'Validation'
WebUI.comment('Validation')

'Memastikan button confirm tidak dapat di klik'
WebUI.verifyElementNotClickable(findTestObject('Website/Cart Page/Modal/Detail Pembayaran/button_confirm_modal'), FailureHandling.STOP_ON_FAILURE)

'Popup detail pembayaran tampil dan button tidak dapat di klik'
CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'('2')

'Klik button close modal'
WebUI.click(findTestObject('Website/Cart Page/Modal/Detail Pembayaran/button_close_modal'))

