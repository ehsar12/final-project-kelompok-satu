import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

'Pre-condition'
WebUI.comment('Pre-condition')

'Berada pada halaman detail event'
CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'('1')

'Steps'
WebUI.comment('Steps')

'Klik button Beli Tiket'
WebUI.click(findTestObject('Website/Detail Event/button_beli_tiket'))

'Validation'
WebUI.comment('Validation')

'Memastikan modal success tampil'
WebUI.verifyElementPresent(findTestObject('Website/Modal Success/modal'), 1)

'Mengambil pesan pada modal success'
message = WebUI.getText(findTestObject('Website/Modal Success/modal_message'))

'Memastikan pesan yang tampil adalah "add to cart success"'
assert message == 'add to cart success'

'Berhasil tampil modal success dengan pesan add to cart success'
CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'('2')
