import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.WebElement as WE
import com.kms.katalon.core.webui.common.WebUiCommonHelper as helper

'Pre-Condition'
WebUI.comment('Pre-condition')

'Arahkan ke halaman utama'
WebUI.navigateToUrl(GlobalVariable.url)

'Berada pada halaman utama'
CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'('1')

'Steps'
WebUI.comment('Steps')

'Klik button profile'
WebUI.click(findTestObject('Website/Home Page/menu_is_logged'))

'Tampil poopup menu'
CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'('2')

'Klik menu checkout'
WebUI.click(findTestObject('Website/Home Page/menu_checkout'))

'Validation'
WebUI.comment('Validation')

'Memastikan header halaman keranjang tampil'
WebUI.verifyElementPresent(findTestObject('Website/Cart Page/header_cart'), 1)

'Menambahkan url halaman keranjang ke variabel global'
GlobalVariable.cartPage = WebUI.getUrl()

'Berhasil diarahkan ke halaman keranjang'
CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'('3')

