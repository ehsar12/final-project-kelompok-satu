import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.context.TestCaseContext
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

'Buka Browser dan masuk ke url'
WebUI.openBrowser(GlobalVariable.url)

'Maximaze Window Browser'
WebUI.maximizeWindow()

'Precondition'
WebUI.comment('Pre-condition')

'Memastikan header home page tampil'
WebUI.verifyElementPresent(findTestObject('Website/Home Page/header'), 1)

'Menunggu menu event dapat di klik'
WebUI.waitForElementClickable(findTestObject('Website/Home Page/menu_events'), 1)

'Berada pada home page'
CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'('1')

'Steps'
WebUI.comment('Steps')

'Klik menu Events'
WebUI.click(findTestObject('Website/Home Page/menu_events'))

'Validation'
WebUI.comment('Validation')

'Memastikan header pada halaman event tampil'
WebUI.verifyElementPresent(findTestObject('Website/Event Page/header_menu_event'), 1)

'Masukkan url event page ke variabel global'
GlobalVariable.eventPageUrl = WebUI.getUrl()

'Berhasil diarahkan ke halaman Events'
CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'('2')

