import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import internal.GlobalVariable as GlobalVariable

'Precondition'
WebUI.comment('Pre-condition')

WebDriver driver = DriverFactory.getWebDriver()

TestObject cbox = findTestObject('Website/Cart Page/checkbox')

'Menghitung jumlah item pada keranjang'
def numElem1 = driver.findElements(By.xpath(cbox.findPropertyValue('xpath'))).size()

'Berada pada halaman keranjang'
CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'('1')

'Steps'
WebUI.comment('Steps')

'Klik button remove pada event'
WebUI.click(findTestObject('Website/Cart Page/button_remove'))

'Validation'
WebUI.comment('Validation')

'Menghitung jumlah item pada keranjang'
def numElem2 = driver.findElements(By.xpath(cbox.findPropertyValue('xpath'))).size()

'Memastikan jumlah item sekarang kurang dari jumlah item sebelumnya'
assert numElem2 < numElem1

'Berhasil menghapus item event'
CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'('2')