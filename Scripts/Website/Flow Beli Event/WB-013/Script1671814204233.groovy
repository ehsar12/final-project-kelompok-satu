import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

'Validation'
WebUI.comment('Pre-condition')

'Memastikan modal success tampil'
WebUI.verifyElementPresent(findTestObject('Website/Modal Success/modal'), 2)

'Berada pada modal success'
CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'('1')

'Steps'
WebUI.comment('Steps')

'Klik button Close modal'
WebUI.click(findTestObject('Website/Modal Success/button_close_modal'))

'Validation'
WebUI.comment('Validation')

'Memastikan button beli tiket tidak tampil pada halaman detail event'
WebUI.verifyElementNotPresent(findTestObject('Website/Detail Event/button_beli_tiket'), 2)

'Berhasil menutup modal dan diarahkan ke halaman detail event dengan button beli tiket tidak tampil lagi'
CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'('2')