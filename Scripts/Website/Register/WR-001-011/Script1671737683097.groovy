import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.comment(description)

WebUI.openBrowser(GlobalVariable.url)

WebUI.maximizeWindow()

WebUI.click(findTestObject('Website/Home Page/button_register'))

WebUI.setText(findTestObject('Object Repository/Website/Register Page/input_nama'), nama)

WebUI.setText(findTestObject('Object Repository/Website/Register Page/input_tanggal_lahir'), tanggal_lahir)

WebUI.setText(findTestObject('Object Repository/Website/Register Page/input_email'), email)

WebUI.setText(findTestObject('Object Repository/Website/Register Page/input_whatsapp'), whatsapp)

WebUI.setText(findTestObject('Object Repository/Website/Register Page/input_kata_sandi'), kata_sandi)

WebUI.setText(findTestObject('Object Repository/Website/Register Page/input_konfirmasi_kata_sandi'), konfirmasi_kata_sandi)

if (syarat_dan_ketentuan == 'Yes') {
    WebUI.check(findTestObject('Object Repository/Website/Register Page/checkbox_syarat_dan_ketentuan'))
}

WebUI.delay(2)

CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'(description + '-1')

WebUI.click(findTestObject('Website/Register Page/button_daftar'))

if(ID == 'WR-001' || ID == 'WR-004' || ID == 'WR-007') {
	WebUI.verifyElementPresent(findTestObject('Object Repository/Website/Register Page/button_daftar'), 2)
	
	WebUI.delay(2)
	
	CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'(description + '-2')
} else if(ID == 'WR-002') {
	WebUI.verifyElementPresent(findTestObject('Object Repository/Website/Register Page/span_email_sudah_terdaftar'), 2)
	
	WebUI.delay(2)
	
	CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'(description + '-2')
} else if(ID == 'WR-003') {
	WebUI.verifyElementPresent(findTestObject('Object Repository/Website/Register Page/span_kata_sandi_tidak_sama'), 2)
	
	WebUI.delay(2)
	
	CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'(description + '-2')
} else if(ID == 'WR-005') {
	WebUI.verifyElementPresent(findTestObject('Object Repository/Website/Register Page/span_batasan_umur'), 2)
	
	WebUI.delay(2)
	
	CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'(description + '-2')
} else if(ID == 'WR-006') {
	WebUI.verifyElementPresent(findTestObject('Object Repository/Website/Register Page/span_min_characters_password'), 2)
	
	WebUI.delay(2)
	
	CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'(description)
} else if(ID == 'WR-008' || ID == 'WR-009' || ID == 'WR-010' || ID == 'WR-011') {
	message = WebUI.getText(findTestObject('Object Repository/Website/Register Page/span_verifikasi_email'), FailureHandling.STOP_ON_FAILURE)
	
	WebUI.takeScreenshot()
	
	assert message == 'Tetap redirect ke halaman Verifikasi Email'
	
	WebUI.delay(2)
	
	CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'(description + '-2')
}

WebUI.closeBrowser()

