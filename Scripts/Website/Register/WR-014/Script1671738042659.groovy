import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.comment('Kirim ulang verifikasi email pendaftaran')

WebUI.openBrowser(GlobalVariable.url)

WebUI.maximizeWindow()

WebUI.click(findTestObject('Website/Home Page/button_register'))

WebUI.setText(findTestObject('Object Repository/Website/Register Page/input_nama'), 'dhika')

WebUI.setText(findTestObject('Object Repository/Website/Register Page/input_tanggal_lahir'), '27-Jul-1995')

WebUI.setText(findTestObject('Object Repository/Website/Register Page/input_email'), 'dhikakb200@gmail.com')

WebUI.setText(findTestObject('Object Repository/Website/Register Page/input_whatsapp'), '08123456789')

WebUI.setText(findTestObject('Object Repository/Website/Register Page/input_kata_sandi'), 'password123')

WebUI.setText(findTestObject('Object Repository/Website/Register Page/input_konfirmasi_kata_sandi'), 'password123')

WebUI.check(findTestObject('Object Repository/Website/Register Page/checkbox_syarat_dan_ketentuan'))

WebUI.click(findTestObject('Website/Register Page/button_daftar'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Website/Register Page/button_kirim_ulang'), 2)

WebUI.delay(62)

WebUI.click(findTestObject('Object Repository/Website/Register Page/button_kirim_ulang'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Website/Register Page/span_kirim_ulang_email_sukses'), 2)

WebUI.delay(2)

CustomKeywords.'com.helper.website.ScreenshotWeb.getSS'('WR-014 Kirim ulang verifikasi email pendaftaran')

WebUI.closeBrowser()

