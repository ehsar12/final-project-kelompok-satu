<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TS_Register</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>10</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>d2a0c527-33e7-41ea-a1ea-1dcb3963283d</testSuiteGuid>
   <testCaseLink>
      <guid>d4f64c84-5480-4821-a7ec-93420736e7aa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/Register/WR-001-011</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>df082c36-f83c-4381-bd29-624733ba4a04</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Website/Register/Register_Invalid</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>df082c36-f83c-4381-bd29-624733ba4a04</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Nama</value>
         <variableId>7cef8933-2099-4507-9730-67ef4031c883</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>df082c36-f83c-4381-bd29-624733ba4a04</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Tanggal Lahir</value>
         <variableId>a07d0f60-2a64-47ce-a00f-4ed1f6b3cf03</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>df082c36-f83c-4381-bd29-624733ba4a04</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>E-mail</value>
         <variableId>9c665f48-05d8-46cf-9a95-0ffdac327889</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>df082c36-f83c-4381-bd29-624733ba4a04</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Whatsapp</value>
         <variableId>0ecf30b2-a463-47b5-9ed9-9dfac659cd8e</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>df082c36-f83c-4381-bd29-624733ba4a04</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Kata Sandi</value>
         <variableId>2e1374bf-21eb-4416-8d78-20502d043b10</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>df082c36-f83c-4381-bd29-624733ba4a04</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Konfirmasi Kata Sandi</value>
         <variableId>e01b30ba-51be-486e-9949-d46818fb2d1c</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>df082c36-f83c-4381-bd29-624733ba4a04</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Syarat dan Ketentuan</value>
         <variableId>f6f27f8f-fe53-4951-90cf-7abbd232b1a2</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>df082c36-f83c-4381-bd29-624733ba4a04</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>ID</value>
         <variableId>ff530faa-da4a-475f-b065-80620aa2967f</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>df082c36-f83c-4381-bd29-624733ba4a04</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Description</value>
         <variableId>df9367fa-70d3-49e9-8a96-c9121380eeab</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>cbe49a01-a66d-4c2b-844c-0ca6f44f703f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/Register/WR-012</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>2134a54b-7c6f-4fd0-87aa-2ffd7c5a47e8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/Register/WR-013</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>b5d1505c-6558-4260-a96e-c2e4f8c0f4d9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/Register/WR-014</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
