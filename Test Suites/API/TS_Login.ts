<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TS_Login</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>dd0520c2-7100-4de6-b1eb-fe63b2c20cb9</testSuiteGuid>
   <testCaseLink>
      <guid>7bfac542-43f3-4fa1-88b0-1adf446054fc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/API/Login/AL-001</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>ae4945fa-ff7f-42a9-9225-4ca7e864cc4d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/API/Login/AL-002</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>bc74fb53-f7a1-467e-8990-838b0962b23c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/API/Login/AL-003</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>342c3b8b-9e12-4353-adcf-41d08216a897</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/API/Login/AL-004-008</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>1ea3403a-21a5-400f-a48e-fed7f0181755</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/API/API_Login_Invalid</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>1ea3403a-21a5-400f-a48e-fed7f0181755</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Email</value>
         <variableId>5c47c55e-89aa-4bc6-bf0e-75cbce7d7a0f</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>1ea3403a-21a5-400f-a48e-fed7f0181755</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Password</value>
         <variableId>4f9be7f9-a1b1-467c-aaba-7e050abd8aab</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>1ea3403a-21a5-400f-a48e-fed7f0181755</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Description</value>
         <variableId>4852aa14-d312-440f-95d4-0bb0d5a525ea</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>2ec7a065-ed46-4da1-b7fe-840109ed882d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/API/Login/AL-009</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
