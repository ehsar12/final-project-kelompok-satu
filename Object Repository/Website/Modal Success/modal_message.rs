<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>modal_message</name>
   <tag></tag>
   <elementGuidId>46028f56-239f-43bc-88ec-cdb8f109740a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#info</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//h3[@id='info']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h3</value>
      <webElementGuid>21296928-3d55-451f-81c9-01bc0e9c37f6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>info</value>
      <webElementGuid>369f3bad-8fb7-489a-9434-681e8a8618c5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>add to cart success</value>
      <webElementGuid>9894a422-b650-42d1-af83-a21ded84b727</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;info&quot;)</value>
      <webElementGuid>30ea4425-f798-4ade-ba3b-64d99408e0af</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//h3[@id='info']</value>
      <webElementGuid>31bf04f1-39f3-4706-809a-3b353aa73c22</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='Modal_Success']/div/div[2]/h3</value>
      <webElementGuid>c6a3e478-a4f7-4cb8-9e53-ff025a6f1f37</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tutup'])[2]/following::h3[1]</value>
      <webElementGuid>fdaed586-72b4-428f-ab26-dd13407c4fba</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Day 4: Workshop'])[4]/preceding::h3[1]</value>
      <webElementGuid>89b990de-7077-49b1-88de-7b39c0e8bfa8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='add to cart success']/parent::*</value>
      <webElementGuid>43eac928-c0a8-48d8-83b0-3fa92ca6d9a4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/h3</value>
      <webElementGuid>1b330369-a6fc-43e2-831d-d55b7b791afd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h3[@id = 'info' and (text() = 'add to cart success' or . = 'add to cart success')]</value>
      <webElementGuid>4749c5d5-0536-40d2-8e6c-f404af4070a2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
