<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>modal</name>
   <tag></tag>
   <elementGuidId>1f28bffd-91e6-499f-a3a6-711a7b93939d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#Modal_Success</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='Modal_Success']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>705ce3e1-8866-463d-be98-a570ba52ab7d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>modal fade  in</value>
      <webElementGuid>0e3c9bd7-ec31-435a-9076-0a6be5042771</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>Modal_Success</value>
      <webElementGuid>1eabc2a5-5150-4076-8b06-7065aaf194ad</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>-1</value>
      <webElementGuid>d0f53d23-e13d-4922-bf2e-c5bb2dab3c36</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>dialog</value>
      <webElementGuid>676f38fb-ce93-4838-857c-4eff71a3f904</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
        
            
             Tutup 
            
            

        
        
            add to cart success
            
            Day 4: Workshop
            
            
            Lihat
                Pembelian Saya
            
            
            
            
            

             Lihat Event Lainnya

        


        
    
</value>
      <webElementGuid>6beebeed-e839-44b1-98a7-4860d6d9b5ca</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;Modal_Success&quot;)</value>
      <webElementGuid>af679361-9ba9-4452-8d52-ad76fabca056</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='Modal_Success']</value>
      <webElementGuid>3e72f40f-e38e-4d48-9383-6e62bfb9e6b3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jadi Pembicara di Event Coding.id'])[1]/following::div[1]</value>
      <webElementGuid>e123c21d-08f2-488e-b2d0-639498688b7f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[6]</value>
      <webElementGuid>96e379c4-c80d-416e-a80a-3aed0eb2192c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'Modal_Success' and (text() = '
    
        
            
             Tutup 
            
            

        
        
            add to cart success
            
            Day 4: Workshop
            
            
            Lihat
                Pembelian Saya
            
            
            
            
            

             Lihat Event Lainnya

        


        
    
' or . = '
    
        
            
             Tutup 
            
            

        
        
            add to cart success
            
            Day 4: Workshop
            
            
            Lihat
                Pembelian Saya
            
            
            
            
            

             Lihat Event Lainnya

        


        
    
')]</value>
      <webElementGuid>29223901-a83a-4b22-8df1-1a4b1db1d43e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
