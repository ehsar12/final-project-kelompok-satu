<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>status_event</name>
   <tag></tag>
   <elementGuidId>16ca5af1-6f3f-4f27-b1ce-07b6411df1ec</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>h5.textOpen</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@data-event = 'Day 3: Predict using Machine Learning']/div/div[2]/h5</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h5</value>
      <webElementGuid>d32d4d8d-daed-4c73-b7ed-9cebae09a6d8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>textOpen</value>
      <webElementGuid>4b6d6ccc-15ec-4d01-8f15-c0f565ebc9ce</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>OPEN until
                                                        18 Nov 2023 11:30 WIB
                                                </value>
      <webElementGuid>2d6aa020-55af-4d8e-8497-67a66086a869</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;loopevent&quot;)/li[@id=&quot;blockListEvent&quot;]/a[1]/div[@class=&quot;cardOuter&quot;]/div[@class=&quot;blockBody&quot;]/h5[@class=&quot;textOpen&quot;]</value>
      <webElementGuid>961612f9-d228-46b1-8c13-e2fdfe2270b8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//a[@data-event = 'Day 3: Predict using Machine Learning']</value>
      <webElementGuid>ef093d19-ebe5-4a48-8251-0d4cfc8d5ea8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='With Ziyad Syauqi Fawwazi'])[1]/following::h5[1]</value>
      <webElementGuid>5b04dacb-d5a9-4317-be72-b1a8229059fa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Day 3: Predict using Machine Learning'])[1]/following::h5[2]</value>
      <webElementGuid>cd6cbd95-ecd1-44f4-aa56-3220ad5af12a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Mini Class'])[1]/preceding::h5[1]</value>
      <webElementGuid>beb11065-6ec1-4956-bca7-fc191ffa9051</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='OPEN']/parent::*</value>
      <webElementGuid>268b6bb6-128a-4260-b1b2-25d468987f93</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/h5</value>
      <webElementGuid>b0082718-6dd9-4339-b831-e339cca77e96</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h5[(text() = 'OPEN until
                                                        18 Nov 2023 11:30 WIB
                                                ' or . = 'OPEN until
                                                        18 Nov 2023 11:30 WIB
                                                ')]</value>
      <webElementGuid>5f044015-23ac-4d31-9dcb-1d8b33befeeb</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
