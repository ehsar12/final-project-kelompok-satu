<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_remove</name>
   <tag></tag>
   <elementGuidId>5234cb51-0279-48b3-9044-a40d76ecde89</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a[(contains(text(), 'Remove') or contains(., 'Remove'))]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//p[(contains(text(), 'Remove') or contains(., 'Remove'))][1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Remove</value>
      <webElementGuid>a78ff47d-3f8a-445e-9d6f-982f5244b682</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>da19c4cf-4381-4732-8061-a9c66afb6e5c</webElementGuid>
   </webElementProperties>
</WebElementEntity>
