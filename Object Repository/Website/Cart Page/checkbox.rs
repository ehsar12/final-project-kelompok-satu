<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>checkbox</name>
   <tag></tag>
   <elementGuidId>799e06f0-333b-434d-9b3f-a0a89b2fbb2e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@type='checkbox' and @id='check']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@type='checkbox' and @id='check']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@type='checkbox' and @id='check']</value>
      <webElementGuid>ead84646-a651-4f06-a33b-3bf5103cb75d</webElementGuid>
   </webElementProperties>
</WebElementEntity>
