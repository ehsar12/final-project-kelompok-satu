<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_method</name>
   <tag></tag>
   <elementGuidId>c3a3253d-1149-41e0-b10f-b301152f37c1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'list-title text-actionable-bold' and (contains(text(), '${method}') or contains(., '${method}'))]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>list-title text-actionable-bold</value>
      <webElementGuid>e63bf3d2-c711-461c-b065-d7fad614fc13</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>${method}</value>
      <webElementGuid>1bcf7835-dbae-4fab-97fb-cf76b0d72ec2</webElementGuid>
   </webElementProperties>
</WebElementEntity>
