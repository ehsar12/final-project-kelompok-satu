<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>header_cart</name>
   <tag></tag>
   <elementGuidId>c71ff7d5-71b2-4fa0-9d1b-d6e350c188af</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//h4[(text() = 'Pembelian Saya' or . = 'Pembelian Saya')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h4</value>
      <webElementGuid>00208fed-0402-4434-b122-527c407a34a8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Pembelian Saya</value>
      <webElementGuid>ef520dfe-2876-450c-8227-5eeb1e3b739e</webElementGuid>
   </webElementProperties>
</WebElementEntity>
