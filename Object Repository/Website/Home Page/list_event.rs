<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>list_event</name>
   <tag></tag>
   <elementGuidId>d132b723-bea4-48de-a261-db4f4ee86516</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@data-event = '${event_name}']</value>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@onclick=&quot;window.location.href='https://demo-app.online/event/day-3-predict-using-machine-learning-uqhdcovt753n';&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>c5e67801-6fe6-4c64-9ef4-eadd2c209070</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-event</name>
      <type>Main</type>
      <value>${event_name}</value>
      <webElementGuid>4ee71b48-c609-4b3a-a0fc-f666c39538c5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>window.location.href='https://demo-app.online/event/day-3-predict-using-machine-learning-uqhdcovt753n';</value>
      <webElementGuid>de8f1689-06f8-49d5-b565-60ecdb565acf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>cardBootcamp list-online-course-item-container </value>
      <webElementGuid>bdd14c17-e014-48ee-9681-094150a752eb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        
                            

                                                            
                                                        

                                
                                    
                                    
                                    Ziyad Syauqi Fawwazi
                                
                                Data Scientist Market Leader Company in Automotive Industry
                                
                            
                        
                        
                        
                        
                            EVENT
                            
                                Day 3: Predict using Machine Learning
                            
                            
                                bersama Ziyad Syauqi Fawwazi, Data Scientist Market Leader Company in Automotive Industry
                            
                            
                                18 Nov 2023,
                                19:30
                                WIB | Via
                                Zoom
                            

                                                            
                                    Open Registration until 18 Nov 2023 11:30 WIB
                                
                            
                                                                                                                                        
                                            Rp. 500.000
                                        Rp.
                                            85.000
                                        
                                                                                                

                        

                    </value>
      <webElementGuid>e016fa9f-544d-4aa3-9900-add7ced0dbf3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-body-homepage&quot;)/div[@class=&quot;online-course-block&quot;]/ul[@class=&quot;list-online-course-container&quot;]/li[@class=&quot;list-online-course-item&quot;]/div[@class=&quot;cardBootcamp list-online-course-item-container&quot;]</value>
      <webElementGuid>95e33f32-fb69-4131-8977-a36949462a29</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@onclick=&quot;window.location.href='https://demo-app.online/event/day-3-predict-using-machine-learning-uqhdcovt753n';&quot;]</value>
      <webElementGuid>af310a0c-385c-4763-a8c5-d30b9da5240a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='main-body-homepage']/div[6]/ul/li/div</value>
      <webElementGuid>dc0a4baa-2ded-40e9-b4e8-3c767b6af46b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Events'])[4]/following::div[1]</value>
      <webElementGuid>2cf1147f-f3cf-4197-ba92-3022341c6268</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lihat Semua Course'])[1]/following::div[2]</value>
      <webElementGuid>7d282adb-11b9-4167-944b-0210b383fbf3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[6]/ul/li/div</value>
      <webElementGuid>72d8e00b-471a-4952-ac4d-5e9410ea7363</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                        
                            

                                                            
                                                        

                                
                                    
                                    
                                    Ziyad Syauqi Fawwazi
                                
                                Data Scientist Market Leader Company in Automotive Industry
                                
                            
                        
                        
                        
                        
                            EVENT
                            
                                Day 3: Predict using Machine Learning
                            
                            
                                bersama Ziyad Syauqi Fawwazi, Data Scientist Market Leader Company in Automotive Industry
                            
                            
                                18 Nov 2023,
                                19:30
                                WIB | Via
                                Zoom
                            

                                                            
                                    Open Registration until 18 Nov 2023 11:30 WIB
                                
                            
                                                                                                                                        
                                            Rp. 500.000
                                        Rp.
                                            85.000
                                        
                                                                                                

                        

                    ' or . = '
                        
                            

                                                            
                                                        

                                
                                    
                                    
                                    Ziyad Syauqi Fawwazi
                                
                                Data Scientist Market Leader Company in Automotive Industry
                                
                            
                        
                        
                        
                        
                            EVENT
                            
                                Day 3: Predict using Machine Learning
                            
                            
                                bersama Ziyad Syauqi Fawwazi, Data Scientist Market Leader Company in Automotive Industry
                            
                            
                                18 Nov 2023,
                                19:30
                                WIB | Via
                                Zoom
                            

                                                            
                                    Open Registration until 18 Nov 2023 11:30 WIB
                                
                            
                                                                                                                                        
                                            Rp. 500.000
                                        Rp.
                                            85.000
                                        
                                                                                                

                        

                    ')]</value>
      <webElementGuid>ab8d37f1-0c81-4a18-8efd-81e9a5ec4cbb</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
