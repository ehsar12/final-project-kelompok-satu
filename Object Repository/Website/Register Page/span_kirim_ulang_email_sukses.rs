<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_kirim_ulang_email_sukses</name>
   <tag></tag>
   <elementGuidId>397e1852-9447-4b5d-86c2-b0f94e04a5db</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.alert.alert-success</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Verifikasi Email'])[1]/following::div[6]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>dddf41d8-9dc1-4076-b527-8f67373cfd6f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>alert alert-success</value>
      <webElementGuid>187b039d-5acf-42fa-ba27-7195f3b400db</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>alert</value>
      <webElementGuid>ef4f3a46-ed06-4970-a5b2-81e94cddc8ec</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                            Link verifikasi baru telah dikirim ke alamat email Anda. (mohon periksa juga kotak spam)
                                        </value>
      <webElementGuid>fbe25251-434c-4152-89a5-5792b0e4f50c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;no-htmlimports no-flash no-proximity no-applicationcache blobconstructor blob-constructor cookies cors customprotocolhandler dataview eventlistener geolocation history no-ie8compat json notification queryselector serviceworker customevent postmessage svg templatestrings typedarrays websockets no-xdomainrequest webaudio webworkers no-contextmenu cssall audio canvas canvastext contenteditable emoji olreversed no-userdata video no-vml webanimations webgl adownload audioloop canvasblending todataurljpeg todataurlpng todataurlwebp canvaswinding no-ambientlight hashchange inputsearchevent pointerevents hiddenscroll no-mathml unicoderange no-touchevents unicode no-batteryapi no-battery-api crypto no-dart gamepads fullscreen indexeddb indexeddb-deletedatabase intl pagevisibility performance pointerlock quotamanagement requestanimationframe raf vibrate no-webintents no-lowbattery getrandomvalues backgroundblendmode cssanimations backdropfilter backgroundcliptext appearance exiforientation audiopreload&quot;]/body[1]/div[@class=&quot;wm-main-wrapper&quot;]/div[@class=&quot;wm-main-section&quot;]/div[@class=&quot;container&quot;]/div[1]/div[@class=&quot;row justify-content-center&quot;]/div[@class=&quot;card&quot;]/div[@class=&quot;row justify-content-center&quot;]/div[@class=&quot;col-md-12&quot;]/div[@class=&quot;card&quot;]/div[@class=&quot;alert alert-success&quot;]</value>
      <webElementGuid>50f7d585-2bc4-480c-a380-82c5370b5e52</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Verifikasi Email'])[1]/following::div[6]</value>
      <webElementGuid>567b0f6c-8817-4eae-a9a9-5d832cdc82ab</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Verifikasi alamat email Anda'])[1]/preceding::div[1]</value>
      <webElementGuid>e3529d1e-c80d-445d-9a27-7ee1919c6644</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kirim Ulang'])[1]/preceding::div[2]</value>
      <webElementGuid>4d759f59-fb4a-4f7a-8823-fb9a19796d73</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Link verifikasi baru telah dikirim ke alamat email Anda. (mohon periksa juga kotak spam)']/parent::*</value>
      <webElementGuid>c331ed52-3fbf-4212-b9fb-f8fcbbb4d2cf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div/div[2]/div/div/div/div/div</value>
      <webElementGuid>dca9ed48-53b1-4bcc-97c3-28ae3db1ab99</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                                            Link verifikasi baru telah dikirim ke alamat email Anda. (mohon periksa juga kotak spam)
                                        ' or . = '
                                            Link verifikasi baru telah dikirim ke alamat email Anda. (mohon periksa juga kotak spam)
                                        ')]</value>
      <webElementGuid>1c281d5c-3491-4293-bf9c-18f1785c97d7</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
