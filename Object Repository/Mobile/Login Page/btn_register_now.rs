<?xml version="1.0" encoding="UTF-8"?>
<MobileElementEntity>
   <description></description>
   <name>btn_register_now</name>
   <tag></tag>
   <elementGuidId>2bd32828-070e-4cb2-bc94-14829ef05524</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <locator>//*[@class = 'android.widget.TextView' and (@text = 'Register, now!' or . = 'Register, now!')]</locator>
   <locatorStrategy>XPATH</locatorStrategy>
</MobileElementEntity>
